import cv2
import os

def capture_frames(video_path, start_time, end_time, capture_number, output_folder):
    # Open the video
    cap = cv2.VideoCapture(video_path)

    # Check if the video was opened successfully
    if not cap.isOpened():
        print(f"Error opening the video: {video_path}")
        return

    # Create the output folder if it doesn't exist
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Get the base name of the video without extension
    video_base_name = os.path.splitext(os.path.basename(video_path))[0]

    # Seek to the desired start time in the video
    cap.set(cv2.CAP_PROP_POS_MSEC, start_time * 1000)

    frame_count = 0
    while cap.isOpened():
        ret, frame = cap.read()

        if ret:
            # Break the loop when the end time is reached or the desired frames are captured
            if cap.get(cv2.CAP_PROP_POS_MSEC) / 1000 > end_time or frame_count >= capture_number:
                break

            # Write the frame to the output file if within the desired time range
            if start_time <= cap.get(cv2.CAP_PROP_POS_MSEC) / 1000 <= end_time:
                image_path = os.path.join(output_folder, f'{video_base_name}.jpg')
                cv2.imwrite(image_path, frame)
                frame_count += 1
        else:
            break

    # Release resources
    cap.release()

    print(f"Video frame capturing completed for: {video_path}")

def process_all_videos(video_folder, start_time, end_time, capture_number, output_folder):
    video_files = [f for f in os.listdir(video_folder) if f.endswith('.mp4')]
    for video_file in video_files:
        video_path = os.path.join(video_folder, video_file)
        capture_frames(video_path, start_time, end_time, capture_number, output_folder)

# Example usage
video_folder = 'Data'  # Specify the folder containing the videos
start_time = 0  # seconds
end_time = 5   # seconds
capture_number = 5  # number of frames to capture (1 frame for the video's name)
output_folder = 'CapturedFrames'  # Specify the folder where captured frames will be saved

# Run the process on all videos in the folder
process_all_videos(video_folder, start_time, end_time, capture_number, output_folder)


