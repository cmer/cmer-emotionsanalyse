import pandas as pd

# Read the input CSV file
input_file = 'video_emotion_results.csv'
df = pd.read_csv(input_file)

# Split the "Name" column and store the first part in a new column
df['Name'] = df['Name'].str.split('-').str[0]

# Define the lookup dictionary
Lookup = {
    'Bartsch': ['Linke', 'M'],
    'Chrupalla': ['Afd', 'M'],
    'Droege': ['Gruenen', 'W'],
    'Duerr': ['Fdp', 'M'],
    'Haselmann': ['Gruenen', 'W'],
    'Merz': ['Cdu', 'M'],
    'Mohamed Ali': ['Linke', 'W'],
    'Muetzenich': ['Spd', 'M'],
    'Weidel': ['Afd', 'W']
}

# Create empty columns for 'partei' and 'geschlecht'
df['partei'] = ''
df['geschlecht'] = ''

# Map values from the Lookup dictionary based on 'Name'
for name, (partei, geschlecht) in Lookup.items():
    df.loc[df['Name'].str.contains(name, case=False), 'partei'] = partei
    df.loc[df['Name'].str.contains(name, case=False), 'geschlecht'] = geschlecht

# Save the modified DataFrame to a new CSV file
output_file = 'output.csv'
df.to_csv(output_file, index=False)

print("CSV file with split video names, partei, and geschlecht saved as 'output.csv'")






def group_csv_by_column(csv_file, group_by_column, output_csv_file):
    try:
        # Read the CSV file into a DataFrame
        df = pd.read_csv(csv_file)

        # Group the data by the specified column
        grouped = df.groupby(group_by_column)

        # You can perform operations on the grouped data if needed
        # For example, you can calculate the mean of numeric columns for each group
        # grouped_mean = grouped.mean()

        # Save the grouped data as a new CSV file
        for group_name, group_df in grouped:
            group_df.to_csv(f"{output_csv_file}_{group_name}.csv", index=False)

        return "Grouped data saved successfully."

    except Exception as e:
        return f"An error occurred: {str(e)}"

# Example usage:
csv_file = 'output.csv'       # Replace with the path to your CSV file
group_by_column = 'Name'         # Replace with the column name you want to group by
output_csv_file = 'grouped_data' # Replace with the desired output CSV file name (without extension)

result_message = group_csv_by_column(csv_file, group_by_column, output_csv_file)

print(result_message)




