import os
import shutil
import cv2
from keras.models import load_model
import numpy as np
from sample_data.datasets import get_labels
from sample_data.inference import detect_faces
from sample_data.inference import apply_offsets
from sample_data.inference import load_detection_model
from sample_data.inference import load_image
from sample_data.preprocessor import preprocess_input
import pandas as pd

def most_frequent(List):
    if not List:
        return None
    return max(set(List), key=List.count)

def get_most_frequent_emotion(dict_):
    emotions = []
    for frame_nmr in dict_.keys():
        for face_nmr in dict_[frame_nmr].keys():
            emotions.append(dict_[frame_nmr][face_nmr]['emotion'])
    return most_frequent(emotions)

def process_video(video_path):
    # Parameters for loading data and models
    detection_model_path = 'D:\Studium\Master\CER\CERM\cmer-emotionsanalyse\trained_models\detection_models\detection_models\fer2013_mini_XCEPTION.102-0.66.hdf5'
    emotion_model_path = 'D:\Studium\Master\CER\CERM\cmer-emotionsanalyse\trained_models\detection_models\detection_models\haarcascade_frontalface_default.xml'
    emotion_labels = get_labels('fer2013')

    # Hyperparameters for bounding box shape
    emotion_offsets = (0, 0)

    # Load models
    face_detection = load_detection_model(detection_model_path)
    emotion_classifier = load_model(emotion_model_path, compile=False)

    # Get input model shapes for inference
    emotion_target_size = emotion_classifier.input_shape[1:3]

    frames_dir = './tmp'

    if os.path.exists(frames_dir):
        shutil.rmtree(frames_dir)
    os.mkdir(frames_dir)

    # Capture video frames
    cap = cv2.VideoCapture(video_path)
    frame_number = 0
    output = {}

    while True:
        ret, frame = cap.read()
        if not ret:
            break

        # Save each frame as an image
        frame_filename = os.path.join(frames_dir, f'frame_{frame_number:010d}.jpg')
        cv2.imwrite(frame_filename, frame)

        # Load and process the frame
        gray_image = load_image(frame_filename, grayscale=True)
        gray_image = np.squeeze(gray_image)
        gray_image = gray_image.astype('uint8')
        faces = detect_faces(face_detection, gray_image)

        tmp = {}

        for face_coordinates_, face_coordinates in enumerate(faces):
            x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
            gray_face = gray_image[y1:y2, x1:x2]

            try:
                gray_face = cv2.resize(gray_face, emotion_target_size)
            except:
                continue

            gray_face = preprocess_input(gray_face, True)
            gray_face = np.expand_dims(gray_face, 0)
            gray_face = np.expand_dims(gray_face, -1)
            emotion_label_arg = np.argmax(emotion_classifier.predict(gray_face))
            emotion_text = emotion_labels[emotion_label_arg]

            tmp[face_coordinates_] = {'emotion': emotion_text, 'score': np.amax(emotion_classifier.predict(gray_face))}

        output[frame_number] = tmp
        frame_number += 1

    cap.release()
    cv2.destroyAllWindows()

    return output, get_most_frequent_emotion(output)

if __name__ == "__main__":
    video_dir = 'D:\Studium\Master\CER\CERM\cmer-emotionsanalyse\data'
    output_dir = 'D:\Studium\Master\CER\CERM\cmer-emotionsanalyse'

    # Create the output directory if it doesn't exist
    os.makedirs(output_dir, exist_ok=True)

    result_data = []  # Store video names and their corresponding emotions

    for video_filename in os.listdir(video_dir):
        if video_filename.endswith('.mp4'):
            video_path = os.path.join(video_dir, video_filename)
            output, most_frequent_emotion = process_video(video_path)

            # Get the video name without the tail
            video_name = os.path.splitext(os.path.basename(video_filename))[0]

            result_data.append([video_name, most_frequent_emotion])

    # Create a DataFrame from the result_data list
    df = pd.DataFrame(result_data, columns=['Name', 'Most Frequent Emotion'])

    # Save the DataFrame to a CSV file
    csv_filename = os.path.join(output_dir, 'video_emotion_results.csv')
    df.to_csv(csv_filename, index=False)

    print(f'Results saved to {csv_filename}')
